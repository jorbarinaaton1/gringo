import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  isLoading = true;
  title = 'gringo';

  ngOnInit(): void {
    setTimeout(
      () => {
        this.isLoading = false;
      }, 2500
    );
  }


}
