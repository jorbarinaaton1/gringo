import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoneyToMobileComponent } from './money-to-mobile.component';

describe('MoneyToMobileComponent', () => {
  let component: MoneyToMobileComponent;
  let fixture: ComponentFixture<MoneyToMobileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoneyToMobileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoneyToMobileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
