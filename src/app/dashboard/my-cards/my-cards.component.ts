import { Component, OnInit } from '@angular/core';
import { DashboardServiceService } from '../dashboard-service.service';

@Component({
  selector: 'app-my-cards',
  templateUrl: './my-cards.component.html',
  styleUrls: ['./my-cards.component.scss']
})
export class MyCardsComponent implements OnInit {
  userCardDetail: any;

  constructor(private dashBoardService: DashboardServiceService) { }

  ngOnInit(): void {
    this.getCardDetail();
  }

  getCardDetail(): void {
    this.dashBoardService.getCardDetail().subscribe(
      successData => {
        this.userCardDetail = successData;
      },
      (error) => {
        console.log('error occurred', error);
        // error notifier
      }
    );

  }

}
