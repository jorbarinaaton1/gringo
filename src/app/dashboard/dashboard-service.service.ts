import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DashboardServiceService {

  constructor(private httpClient: HttpClient) { }

  getUserDetail(): Observable<any> {
    const url = `${environment.apiUrl}/userDetail`;
    console.log(url);
    return this.httpClient.get(url);
  }

  getCardDetail(): Observable<any> {
    const url = `${environment.apiUrl}/cardDetails`;
    console.log(url);
    return this.httpClient.get(url);
  }

  getTransactionRecords(): Observable<any> {
    const url = `${environment.apiUrl}/transactionsDetails`;
    console.log(url);
    return this.httpClient.get(url);
  }

  getMoneyBoxProgress(): Observable<any> {
    const url = `${environment.apiUrl}/boxProgress`;
    console.log(url);
    return this.httpClient.get(url);
  }

  getOtherUserData(): Observable<any> {
    const url = `${environment.apiUrl}/makeTransaction`;
    console.log(url);
    return this.httpClient.get(url);
  }

  getAnalyticsData(): Observable<any> {
    const url = `${environment.apiUrl}/analytics`;
    console.log(url);
    return this.httpClient.get(url);
  }
}
