import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DashboardServiceService } from '../dashboard-service.service';

@Component({
  selector: 'app-send-money',
  templateUrl: './send-money.component.html',
  styleUrls: ['./send-money.component.scss']
})
export class SendMoneyComponent implements OnInit {
  sendMoney: any;

  constructor(private dashBoardService: DashboardServiceService) { }

  ngOnInit(): void {
    this.getOtherUserData();
  }

  getOtherUserData(): void {
    this.dashBoardService.getOtherUserData().subscribe(
      successData => {
        this.sendMoney = successData;
      },
      (error) => {
        console.log('error occurred', error);
        // error notifier
      }
    );

  }


}
