import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { DashboardServiceService } from '../dashboard-service.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {
  analyticsData: any;

  constructor(private dashBoardService: DashboardServiceService) { }


  ngOnInit(): void {
    this.getAnalytics();
  }

  getAnalytics(): void {
    this.dashBoardService.getAnalyticsData().subscribe(
      successData => {
        this.analyticsData = successData;
        this.populateChart();
      },
      (error) => {
        console.log('error occurred', error);
        // error notifier
      }
    );

  }

  populateChart(): void {
     // tslint:disable-next-line: no-unused-expression
     new Chart('myChart', {
      type: 'line',
      data: {
        labels: this.analyticsData.labels,
        datasets: [
          {
            data: this.analyticsData.dataOne,
            borderWidth: 3,
            borderColor: '#49a1f6',
            backgroundColor: '#f5f9fe63',
            pointBorderColor: 'white',
            pointRadius: 10,
            pointBackgroundColor: '#2493fe',
            pointBorderWidth: 10
          },
          {
            data: this.analyticsData.dataTwo,
            borderWidth: 1,
            borderColor: '#c5c8cd',
            backgroundColor: '#f5f9fe14'
          }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false,
        },
        scales: {
          yAxes: [{
              stacked: false
          }]
      }
      }
    });
  }

}
