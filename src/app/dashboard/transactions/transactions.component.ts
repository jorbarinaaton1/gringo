import { Component, OnInit } from '@angular/core';
import { DashboardServiceService } from '../dashboard-service.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  transactionRecords: any[];

  constructor(private dashBoardService: DashboardServiceService) { }

  ngOnInit(): void {
    this.getTransactionRecord();
  }

  getTransactionRecord(): void {
    this.dashBoardService.getTransactionRecords().subscribe(
      successData => {
        this.transactionRecords = successData;
      },
      (error) => {
        console.log('error occurred', error);
        // error notifier
      }
    );

  }

}
