import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { LayoutComponent } from './layout/layout.component';

import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MyCardsComponent } from './my-cards/my-cards.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { MoneyBoxComponent } from './money-box/money-box.component';
import { AnalyticsComponent } from './analytics/analytics.component';
import { MoneyToMobileComponent } from './money-to-mobile/money-to-mobile.component';
import { SendMoneyComponent } from './send-money/send-money.component';


@NgModule({
  declarations: [LayoutComponent, MyCardsComponent, TransactionsComponent, MoneyBoxComponent, AnalyticsComponent, MoneyToMobileComponent, SendMoneyComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgxIntlTelInputModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class DashboardModule { }
