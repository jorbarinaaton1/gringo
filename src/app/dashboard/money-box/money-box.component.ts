import { Component, OnInit } from '@angular/core';
import { DashboardServiceService } from '../dashboard-service.service';

@Component({
  selector: 'app-money-box',
  templateUrl: './money-box.component.html',
  styleUrls: ['./money-box.component.scss']
})
export class MoneyBoxComponent implements OnInit {
  moneyBoxProgress = 0;

  constructor(private dashBoardService: DashboardServiceService) { }

  ngOnInit(): void {
    this.getMoneyBoxProgress();
  }

  getMoneyBoxProgress(): void {
    this.dashBoardService.getMoneyBoxProgress().subscribe(
      successData => {
        this.moneyBoxProgress = successData.value;
      },
      (error) => {
        console.log('error occurred', error);
        // error notifier
      }
    );

  }

}
