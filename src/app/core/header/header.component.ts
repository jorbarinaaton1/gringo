import { Component, OnInit } from '@angular/core';
import { DashboardServiceService } from 'src/app/dashboard/dashboard-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  userDetail: any;

  constructor(private dashBoardService: DashboardServiceService) { }

  ngOnInit(): void {
    this.getUserDetail();
  }

  getUserDetail(): void {
    this.dashBoardService.getUserDetail().subscribe(
      successData => {
        this.userDetail = successData;
      },
      (error) => {
        console.log('error occurred', error);
        // error notifier
      }
    );
  }

}
